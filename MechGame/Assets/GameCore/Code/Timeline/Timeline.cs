﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

namespace GameCore.Utility
{
    [Serializable]
    public class Timeline<T>
    {
        HashSet<TimelineValue<T>> timeLineValues;

        public Timeline()
        {
            timeLineValues = new HashSet<TimelineValue<T>>();
        }

        public void InsertValue(TimelineValue<T> timelineValue)
        {
            timeLineValues.Add(timelineValue);
        }

        public List<TimelineValue<T>> GetAllValues()
        {
            return timeLineValues.ToList();
        }
    }

    [Serializable]
    public class TimelineValue<T>
    {
        public double Time
        {
            get; private set;
        }
        public T Value
        {
            get; private set;
        }
        public TimelineValue(double time, T value)
        {
            Time = time;
            Value = value;
        }
        public TimelineValue() { }
    }

}