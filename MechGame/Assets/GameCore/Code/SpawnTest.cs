﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Physics;
using Unity.Transforms;
using Unity.Mathematics;
using GameCore.Projectiles;
using GameCore.Movement;
using GameCore.Damage;
using GameCore.Input;
using GameCore.Raycasting;

namespace MechGame
{
    public class SpawnTest : MonoBehaviour
    {
        EntityManager entityManager;

        [SerializeField]
        GameObject entityPrefab;

        [SerializeField]
        GameObject damageablePrefab;

        Entity[] instances;
        Entity[] shooters;

        public int totalEntities = 10;

        private void Awake()
        {
            if(World.Active == null)
            {
                World.Active = new World("Game-World");
            }
        }

        void Start()
        {
            entityManager = World.Active.EntityManager;

            var entity = GameObjectConversionUtility.ConvertGameObjectHierarchy(entityPrefab, World.Active);
            var damageableEntity = GameObjectConversionUtility.ConvertGameObjectHierarchy(damageablePrefab, World.Active);

            var prefab = Resources.Load<GameObject>("Projectiles/CubeBullet");
            var projectilePrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(prefab, World.Active);

            instances = new Entity[totalEntities];
            shooters = new Entity[totalEntities];

            for (int i = 0; i < totalEntities; i++)
            {
                var instance = entityManager.Instantiate(entity);
                var shooter = instance;
                entityManager.SetComponentData(instance, new Translation { Value = new float3(i*2f, 1f, 0f) });
                entityManager.SetComponentData(instance, new MovementInput { TranslationAxis = new float3(0f, 0f, 1f), RotationAxis = new float3(1f, 0f, 0f) });
                entityManager.SetComponentData(instance, new GroundMovementData
                {
                    AccelerationForward = 5f,
                    AccelerationBackward = 5f,
                    AccelerationStrafe = 5f,
                    AccelerationTurn = 5f,
                    MaxSpeedBackward = 4f,
                    MaxSpeedForward = 6f,
                    MaxSpeedStrafe = 4f,
                    MaxSpeedTurn = 60f
                });
                entityManager.SetComponentData(instance, new PhysicsVelocity { Linear = new float3(0f, 0f, 0f), Angular = new float3(0f, 0f, 0f) });

                //entityManager.AddComponentData(shooter, new ProjectileShooter(projectilePrefab,
                //    new Projectile(shooter, 30f, 1000f, new float3(0f, 0f, 0f), 3f), 0.05f, 0.5f, 0));
                entityManager.AddComponentData(shooter, new RaycastShooter(0.2f, 1000f, 0.5f));
                entityManager.AddComponentData(shooter, new TriggerInput { Triggered = false });
                entityManager.AddComponentData(shooter, new DamageDealer { Damage = 10, ArmorPiercingDamage = 0 });

                var damageable = entityManager.Instantiate(damageableEntity);
                entityManager.SetComponentData(damageable, new Translation { Value = new float3(0f, 0.5f, 8f) });
                entityManager.AddComponentData(damageable, new Damageable { Armor = 1, Damage = 0, Durability = 100 });
                instances[i] = instance;
                shooters[i] = shooter;
            }
        }

        void Update()
        {
            for (int i = 0; i < instances.Length; i++)
            {
                entityManager.SetComponentData(instances[i], new MovementInput
                {
                    TranslationAxis = new float3(0f, 0f, Input.GetAxis("Vertical")),
                    RotationAxis = new float3(Input.GetAxis("Horizontal"), 0f, 0f)
                });
                entityManager.SetComponentData(shooters[i], new TriggerInput
                {
                    Triggered = Input.GetMouseButton(0)
                });
            }
        }
    }
}