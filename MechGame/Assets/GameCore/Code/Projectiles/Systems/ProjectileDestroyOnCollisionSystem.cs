﻿using UnityEngine;
using Unity.Entities;
using Unity.Jobs;

namespace GameCore.Projectiles
{
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    public class ProjectileDestroyOnCollisionSystem : JobComponentSystem
    {
        private BeginSimulationEntityCommandBufferSystem entityCommandBufferSystem;

        protected override void OnCreate()
        {
            base.OnCreate();
            entityCommandBufferSystem = World.GetOrCreateSystem<BeginSimulationEntityCommandBufferSystem>();
        }

        struct DestroyProjectilesOnCollisionJob : IJobForEachWithEntity<ProjectileCollisionEvent>
        {
            public readonly EntityCommandBuffer.Concurrent CommandBuffer;

            public DestroyProjectilesOnCollisionJob(EntityCommandBuffer.Concurrent commandBuffer)
            {
                CommandBuffer = commandBuffer;
            }

            public void Execute(Entity entity, int index, ref ProjectileCollisionEvent collision)
            {
                CommandBuffer.DestroyEntity(index, collision.ProjectileEntity);
            }
        }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var job = new DestroyProjectilesOnCollisionJob(entityCommandBufferSystem.CreateCommandBuffer().ToConcurrent()).Schedule(this, inputDeps);
            entityCommandBufferSystem.AddJobHandleForProducer(job);

            return job;
        }
    }
}