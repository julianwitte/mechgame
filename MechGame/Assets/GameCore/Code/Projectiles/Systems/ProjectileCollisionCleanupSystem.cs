﻿using UnityEngine;
using System.Collections;
using Unity.Entities;

namespace GameCore.Projectiles
{
    [UpdateInGroup(typeof(InitializationSystemGroup))]
    public class ProjectileCollisionCleanupSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((Entity entity, ref ProjectileCollisionEvent collisionEvent) =>
            {
                PostUpdateCommands.DestroyEntity(entity);
            });
        }
    }
}