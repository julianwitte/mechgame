﻿//using Unity.Entities;
//using Unity.Transforms;
//using Unity.Physics;
//using Unity.Jobs;
//using Unity.Mathematics;
//using Unity.Burst;
//using Unity.Collections;

//namespace GameCore.Projectiles
//{
//    [UpdateInGroup(typeof(SimulationSystemGroup))]
//    public class ProjectileVelocitySystem : JobComponentSystem
//    {
//        [BurstCompile]   
//        struct ProjectileJob : IJobForEachWithEntity<Projectile, Translation, Rotation, PhysicsVelocity>
//        {
//            public void Execute(Entity entity, int index, [ReadOnly] ref Projectile projectile, [ReadOnly] ref Translation translation, [ReadOnly] ref Rotation rotation, ref PhysicsVelocity velocity)
//            {
//                if (math.distance(projectile.Origin, translation.Value) < projectile.EffectiveDistance)
//                {
//                    float3 direction = math.forward(rotation.Value);
//                    velocity.Linear = direction * projectile.Speed;
//                }
//            }
//        }

//        protected override JobHandle OnUpdate(JobHandle inputDeps)
//        {
//            var job = new ProjectileJob()
//            {
//            }.Schedule(this, inputDeps);

//            return job;
//        }
//    }
//}