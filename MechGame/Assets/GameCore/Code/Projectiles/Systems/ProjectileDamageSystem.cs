﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using GameCore.Damage;

namespace GameCore.Projectiles
{
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    public class ProjectileDamageSystem : JobComponentSystem
    {
        private BeginSimulationEntityCommandBufferSystem entityCommandBufferSystem;

        protected override void OnCreate()
        {
            base.OnCreate();
            entityCommandBufferSystem = World.GetOrCreateSystem<BeginSimulationEntityCommandBufferSystem>();
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var job = new ProjectileDamageJob
            (
                entityCommandBufferSystem.CreateCommandBuffer().ToConcurrent()
            ).Schedule(this, inputDeps);

            entityCommandBufferSystem.AddJobHandleForProducer(job);

            return job;
        }
        struct ProjectileDamageJob : IJobForEachWithEntity<ProjectileCollisionEvent>
        {
            public readonly EntityCommandBuffer.Concurrent CommandBuffer;

            public ProjectileDamageJob(EntityCommandBuffer.Concurrent commandBuffer)
            {
                CommandBuffer = commandBuffer;
            }

            public void Execute(Entity entity, int index, ref ProjectileCollisionEvent projectileCollisionEvent)
            {
                var damaged = projectileCollisionEvent.CollisionBody;
                var damageDealer = projectileCollisionEvent.ProjectileData.Shooter;

                var damageEvent = CommandBuffer.CreateEntity(index);
                CommandBuffer.AddComponent(index, damageEvent, new DamageEvent(damageEvent, damaged, damageDealer));
            }
        }

    }
}