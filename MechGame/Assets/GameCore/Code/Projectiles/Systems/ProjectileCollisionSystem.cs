﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Jobs;
using Unity.Transforms;
using Unity.Collections;

namespace GameCore.Projectiles
{
    //[UpdateAfter(typeof(StepPhysicsWorld))]
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    public class ProjectileCollisionSystem : ComponentSystem
    {
        private BuildPhysicsWorld buildPhysicsWorldSystem;
        private StepPhysicsWorld stepPhysicsWorldSystem;

        protected override void OnCreate()
        {
            base.OnCreate();
            buildPhysicsWorldSystem = World.Active.GetOrCreateSystem<BuildPhysicsWorld>();
            stepPhysicsWorldSystem = World.Active.GetOrCreateSystem<StepPhysicsWorld>();
            Entities.ForEach((ref Projectile p) => { }); // To prevent is from running when there are no projectiles.
        }

        protected override void OnUpdate()
        {
            NativeSlice<RigidBody> bodies = buildPhysicsWorldSystem.PhysicsWorld.Bodies;
            CollisionEvents collisionEvents = stepPhysicsWorldSystem.Simulation.CollisionEvents;
            foreach (CollisionEvent collisionEvent in collisionEvents)
            {
                Entity entityA = bodies[collisionEvent.BodyIndices.BodyAIndex].Entity;
                Entity entityB = bodies[collisionEvent.BodyIndices.BodyBIndex].Entity;
                if (EntityManager.HasComponent<Projectile>(entityA))
                {
                    Entity projCollision = EntityManager.CreateEntity();
                    Translation position = EntityManager.GetComponentData<Translation>(entityA);
                    Projectile projectile = EntityManager.GetComponentData<Projectile>(entityA);
                    EntityManager.AddComponentData(projCollision, new ProjectileCollisionEvent
                    {
                        CollisionBody = entityB,
                        Normal = collisionEvent.Normal,
                        Position = position.Value,
                        ProjectileData = projectile,
                        ProjectileEntity = entityA
                    });
                }
            }
        }
        
    }
}