﻿using GameCore.Input;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Transforms;
using UnityEngine;

namespace GameCore.Projectiles
{
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    public class ProjectileShootingSystem : JobComponentSystem
    {
        private BeginSimulationEntityCommandBufferSystem entityCommandBufferSystem;

        struct ShootingJob : IJobForEach<ProjectileShooter, TriggerInput, Translation, Rotation>
        {
            public float Time;
            public EntityCommandBuffer CommandBuffer;

            public void Execute([ReadOnly] ref ProjectileShooter shooter,[ReadOnly] ref TriggerInput input, [ReadOnly] ref Translation pos, [ReadOnly] ref Rotation rot)
            {
                if (input.Triggered && (Time - shooter.LastTriggerTime > shooter.MaxTriggerRate))
                {
                    if (shooter.MaxShotsPerTrigger == 0 || shooter.ShotsSinceTriggered < shooter.MaxShotsPerTrigger)
                    {
                        if (Time - shooter.LastShotTime > shooter.FireRate)
                        {
                            var projectile = CommandBuffer.Instantiate(shooter.ProjectilePrefab);
                            var data = shooter.ProjectileData;
                            data.Origin = pos.Value;
                            var forward = math.forward(rot.Value);
                            CommandBuffer.AddComponent(projectile, data);
                            CommandBuffer.SetComponent(projectile, new Translation { Value = pos.Value + forward });
                            CommandBuffer.SetComponent(projectile, rot);
                            CommandBuffer.AddComponent(projectile, new TimedLife { CreationTime = Time, LifeTime = data.LifeTime });
                            CommandBuffer.SetComponent(projectile, new PhysicsVelocity
                            {
                                Linear = math.rotate(rot.Value, new float3(0f, 0f, data.Speed)),
                                Angular = new float3()
                            });
                            shooter.LastShotTime = Time;
                            shooter.ShotsSinceTriggered++;
                        }
                    }
                }
                else
                {
                    shooter.LastTriggerTime = shooter.LastShotTime;
                    shooter.ShotsSinceTriggered = 0;
                }
            }
        }

        protected override void OnCreate()
        {
            base.OnCreate();
            entityCommandBufferSystem = World.GetOrCreateSystem<BeginSimulationEntityCommandBufferSystem>();
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var job = new ShootingJob
            {
                Time = Time.time,
                CommandBuffer = entityCommandBufferSystem.CreateCommandBuffer()
            }.ScheduleSingle(this, inputDeps);

            entityCommandBufferSystem.AddJobHandleForProducer(job);
            return job;
        }
    }
}