﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Authoring;

namespace GameCore.Projectiles
{
    [System.Serializable]
    public struct ProjectileCollisionEvent : IComponentData
    {
        public Entity ProjectileEntity;
        public Projectile ProjectileData;
        public float3 Position;
        public float3 Normal;
        public Entity CollisionBody;
    }
}