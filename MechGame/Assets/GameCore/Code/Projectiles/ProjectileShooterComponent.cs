﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;

namespace GameCore.Projectiles
{
    [System.Serializable]
    public struct ProjectileShooter : IComponentData
    {
        public float FireRate;
        public float LastShotTime;
        public Entity ProjectilePrefab;
        public Projectile ProjectileData;
        public int ShotsSinceTriggered;
        /// <summary>
        /// Zero means unlimited.
        /// </summary>
        public int MaxShotsPerTrigger;

        public float MaxTriggerRate;
        public float LastTriggerTime;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectilePrefab"></param>
        /// <param name="fireRate"></param>
        /// <param name="maxShotsPerTrigger, 0 = unlimited"></param>
        public ProjectileShooter(Entity projectilePrefab, Projectile projectileData, float fireRate, float maxTriggerRate, int maxShotsPerTrigger = 0)
        {
            ProjectilePrefab = projectilePrefab;
            ProjectileData = projectileData;
            FireRate = fireRate;
            MaxTriggerRate = maxTriggerRate;
            LastTriggerTime = -maxTriggerRate;
            MaxShotsPerTrigger = maxShotsPerTrigger;
            LastShotTime = -FireRate;
            ShotsSinceTriggered = 0;
        }
    }

    public class ProjectileShooterComponent : ComponentDataProxy<ProjectileShooter>
    {

    }
}