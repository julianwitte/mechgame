﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using Unity.Entities;

namespace GameCore.Projectiles
{
    public class ProjectileDefinitions : ScriptableObject
    {
        Dictionary<string, Entity> projectilePrefabs = new Dictionary<string, Entity>();

        [SerializeField]
        List<ProjectileDefinition> projectileDefinitions;

        public Entity GetProjectilePrefab(string projectileID)
        {
            if (projectilePrefabs.ContainsKey(projectileID))
            {
                return projectilePrefabs[projectileID];
            }
            else
            {
                var definition = projectileDefinitions.Find((x) => x.id.Equals(projectileID));
                var prefab = Resources.Load<GameObject>(definition.PrefabPath);
                var entityPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(prefab, World.Active);
                var entityManager = World.Active.EntityManager;

                var projectileDefinition = projectileDefinitions.Find((p) => p.id == projectileID);

                entityManager.SetComponentData(entityPrefab, new Projectile
                {
                    Speed = projectileDefinition.Speed
                });

                projectilePrefabs.Add(definition.id, entityPrefab);
                return entityPrefab;
            }
        }

        [System.Serializable]
        public struct ProjectileDefinition
        {
            public string id;
            public string PrefabPath;
            public float Speed;
        }
    }
}