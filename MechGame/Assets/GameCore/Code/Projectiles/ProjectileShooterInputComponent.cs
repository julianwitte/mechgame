﻿using Unity.Entities;
using System.Collections;

namespace GameCore.Input
{
    [System.Serializable]
    public struct TriggerInput : IComponentData
    {
        public bool Triggered;
    }

    public class TriggerInputComponent : ComponentDataProxy<TriggerInput>
    {

    }
}