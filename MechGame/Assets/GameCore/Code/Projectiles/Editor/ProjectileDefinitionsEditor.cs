﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

namespace GameCore.Projectiles
{
    public class ProjectileDefinitionsEditor
    {
        const string projectileDefinitionsName = "/ProjectileDefinitions.asset";
        [MenuItem("Assets/Create/Game Settings/Projectile Definitions")]
        public static void CreateProjectileSettings()
        {
            var path = EditorPathUtility.ActiveFolder() + projectileDefinitionsName;
            var obj = ScriptableObject.CreateInstance<ProjectileDefinitions>();
            AssetDatabase.CreateAsset(obj, path);
            var createdAsset = AssetDatabase.LoadAssetAtPath<ProjectileDefinitions>(path);
            Selection.activeObject = createdAsset;
        }
    }
}