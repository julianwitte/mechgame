﻿using UnityEngine;
using System.Collections;
using Unity.Entities;
using Unity.Mathematics;

namespace GameCore.Projectiles
{
    [System.Serializable]
    public struct Projectile : IComponentData
    {
        public float Speed;
        public float EffectiveDistance;
        public float3 Origin;
        public float LifeTime;
        public Entity Shooter;

        public Projectile(Entity shooter, float speed, float effectiveDistance, float3 origin, float lifeTime)
        {
            Shooter = shooter;
            Speed = speed;
            EffectiveDistance = effectiveDistance;
            Origin = origin;
            LifeTime = lifeTime;
        }
    }

    public class ProjectileComponent : ComponentDataProxy<Projectile>
    {

    }
}