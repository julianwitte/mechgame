﻿using System;
using System.Collections.Generic;
using System.Linq;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Physics.Authoring;
using UnityEditor;
using UnityEngine;

namespace GameCore.Raycasting
{
    public class RaycastShooterSettingsEditor : Editor
    {
        const string raycastShooterSettingsName = "/RaycastShooterSettings.asset";
        [MenuItem("Assets/Create/Game Settings/Raycast Shooter Settings")]
        public static void CreateProjectileSettings()
        {
            var path = EditorPathUtility.ActiveFolder() + raycastShooterSettingsName;
            var obj = ScriptableObject.CreateInstance<RaycastShooterSettings>();
            AssetDatabase.CreateAsset(obj, path);
            var createdAsset = AssetDatabase.LoadAssetAtPath<RaycastShooterSettings>(path);
            Selection.activeObject = createdAsset;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
        }
    }
}