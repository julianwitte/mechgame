﻿using Unity.Entities;
using Unity.Mathematics;

namespace GameCore.Raycasting
{
    public struct RaycasHitEvent : IComponentData
    {
        public Entity RaycasterEntity;
        public float3 Origin;
        public float3 HitPosition;
        public float3 Normal;
        public Entity HitBody;

        public RaycasHitEvent(Entity raycasterEntity, float3 origin, float3 hitPosition, float3 normal, Entity hitBody)
        {
            RaycasterEntity = raycasterEntity;
            Origin = origin;
            HitPosition = hitPosition;
            Normal = normal;
            HitBody = hitBody;
        }
    }
}