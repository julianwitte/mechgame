﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

namespace GameCore.Raycasting
{
    public struct RaycastShooter : IComponentData
    {
        public float FireRate;
        public float LastShotTime;

        public float MaxRange;

        public float MaxTriggerRate;
        public float LastTriggerTime;
        /// <summary>
        /// Zero means unlimited.
        /// </summary>
        public int MaxShotsPerTrigger;

        public int ShotsSinceTriggered;

        public RaycastShooter(float fireRate, float maxRange, float maxTriggerRate, int maxShotsPerTrigger = 0)
        {
            FireRate = fireRate;
            LastShotTime = -fireRate;
            MaxRange = maxRange;
            MaxTriggerRate = maxTriggerRate;
            LastTriggerTime = -maxTriggerRate;
            MaxShotsPerTrigger = maxShotsPerTrigger;
            ShotsSinceTriggered = 0;
        }
    }

    public class RaycastShooterComponent : ComponentDataProxy<RaycastShooter>
    {

    }
}
