﻿using System.Collections;
using System.Collections.Generic;
using Unity.Physics;
using Unity.Entities;
using UnityEngine;

namespace GameCore.Raycasting
{
    public class RaycastShooterSettings : ScriptableObject
    {
        [SerializeField]
        private int[] collisionLayers;
        public int[] CollisionLayers { get => collisionLayers; }

        [SerializeField]
        private int[] belongsToLayers;
        public int[] BelongsToLayers { get => belongsToLayers; }
    }
}