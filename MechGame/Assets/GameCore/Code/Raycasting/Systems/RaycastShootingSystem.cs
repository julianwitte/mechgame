﻿using GameCore.Input;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;
using RaycastHit = Unity.Physics.RaycastHit;
using Ray = Unity.Physics.Ray;
using Unity.Physics.Extensions;

namespace GameCore.Raycasting
{
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    public class RaycastShootingSystem : JobComponentSystem
    {
        BuildPhysicsWorld buildPhysicsWorld;
        BeginSimulationEntityCommandBufferSystem bufferSystem;

        CollisionFilter collisionFilter;

        protected override void OnCreate()
        {
            base.OnCreate();
            buildPhysicsWorld = World.GetOrCreateSystem<BuildPhysicsWorld>();
            bufferSystem = World.GetOrCreateSystem<BeginSimulationEntityCommandBufferSystem>();
            var raycastShootingSetting = Resources.Load<RaycastShooterSettings>("Settings/RaycastShooterSettings");
            Debug.Log(raycastShootingSetting);

            uint mask = 0;
            for (int i = 0; i < raycastShootingSetting.CollisionLayers.Length; i++)
            {
                mask |= (uint)(1 << raycastShootingSetting.CollisionLayers[i]);
            }
            uint category = 0;
            for (int i = 0; i < raycastShootingSetting.BelongsToLayers.Length; i++)
            {
                category |= (uint)(1 << raycastShootingSetting.BelongsToLayers[i]);
            }
            collisionFilter = new CollisionFilter()
            {
                GroupIndex = 0,
                MaskBits = mask,
                CategoryBits = category
            };

            Debug.Log(collisionFilter.CategoryBits);
            Debug.Log(collisionFilter.MaskBits);
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var job = new RaycastShootingJob(Time.time, buildPhysicsWorld.PhysicsWorld, collisionFilter, bufferSystem.CreateCommandBuffer().ToConcurrent()).Schedule(this, inputDeps);

            bufferSystem.AddJobHandleForProducer(job);

            return job;
        }

        struct RaycastShootingJob : IJobForEachWithEntity<RaycastShooter, TriggerInput, Translation, Rotation>
        {
            private float time;
            private readonly PhysicsWorld physicsWorld;
            private readonly CollisionFilter collisionFilter;
            private readonly EntityCommandBuffer.Concurrent commandBuffer;

            public RaycastShootingJob(float time, PhysicsWorld physicsWorld, CollisionFilter collisionFilter, EntityCommandBuffer.Concurrent commandBuffer)
            {
                this.time = time;
                this.physicsWorld = physicsWorld;
                this.commandBuffer = commandBuffer;
                this.collisionFilter = collisionFilter;
            }

            public void Execute(Entity entity, int index, ref RaycastShooter shooter, [ReadOnly] ref TriggerInput input, [ReadOnly] ref Translation translation, [ReadOnly] ref Rotation rotation)
            {
                if (input.Triggered && (time - shooter.LastTriggerTime > shooter.MaxTriggerRate))
                {
                    if (shooter.MaxShotsPerTrigger == 0 || shooter.ShotsSinceTriggered < shooter.MaxShotsPerTrigger)
                    {
                        if (time - shooter.LastShotTime > shooter.FireRate)
                        {
                            Debug.Log("Ray Cast 3");
                            RaycastInput raycastInput = new RaycastInput()
                            {
                                Ray = new Ray()
                                {
                                    Origin = translation.Value,
                                    Direction = math.forward(rotation.Value) * shooter.MaxRange,
                                },
                                Filter = collisionFilter
                            };

                            RaycastHit hit = new RaycastHit();
                            bool haveHit = physicsWorld.CollisionWorld.CastRay(raycastInput, out hit);
                            if (haveHit)
                            {
                                Debug.Log("Ray Cast 4");
                                Entity e = physicsWorld.Bodies[hit.RigidBodyIndex].Entity;

                                var hitEventEntity = commandBuffer.CreateEntity(index);
                                commandBuffer.AddComponent(index, hitEventEntity, new RaycasHitEvent(entity, translation.Value, hit.Position, hit.SurfaceNormal, e));
                            }
                            shooter.LastShotTime = time;
                            shooter.ShotsSinceTriggered++;
                        }
                    }
                }
                else
                {
                    shooter.LastTriggerTime = shooter.LastShotTime;
                    shooter.ShotsSinceTriggered = 0;
                }
            }
        }
    }
}