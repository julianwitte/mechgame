﻿using UnityEngine;
using System.Collections;
using Unity.Entities;

namespace GameCore.Raycasting
{
    [UpdateInGroup(typeof(InitializationSystemGroup))]
    public class RaycastShootingHitEventCleanupSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((Entity entity, ref RaycasHitEvent hitEvent) =>
            {
                Debug.Log("RaycastShootingHitEventCleanupSystem");
                Debug.Log(hitEvent.Origin);
                Debug.Log(hitEvent.HitPosition);
                Debug.DrawLine(new Vector3(hitEvent.Origin.x, hitEvent.Origin.y, hitEvent.Origin.z), new Vector3(hitEvent.HitPosition.x, hitEvent.HitPosition.y, hitEvent.HitPosition.z), Color.magenta, 0.25f);
                PostUpdateCommands.DestroyEntity(entity);
            });
        }
    }
}