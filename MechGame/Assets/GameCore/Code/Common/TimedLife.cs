﻿using UnityEngine;
using System.Collections;
using Unity.Entities;

namespace GameCore
{
    [System.Serializable]
    public struct TimedLife : IComponentData
    {
        public float CreationTime;
        public float LifeTime;

        public TimedLife(float creationTime, float lifeTime)
        {
            CreationTime = creationTime;
            LifeTime = lifeTime;
        }
    }
}