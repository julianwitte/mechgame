﻿using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Collections;

namespace GameCore
{
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    public class TimedLifeSystem : JobComponentSystem
    {
        BeginSimulationEntityCommandBufferSystem entityCommandBufferSystem;

        protected override void OnCreate()
        {
            base.OnCreate();
            entityCommandBufferSystem = World.GetOrCreateSystem<BeginSimulationEntityCommandBufferSystem>();
        }

        struct TimedLifeJob : IJobForEachWithEntity<TimedLife>
        {
            public float Time;
            public EntityCommandBuffer.Concurrent CommandBuffer;

            public void Execute(Entity entity, int index, [ReadOnly] ref TimedLife timedLife)
            {
                if (Time - timedLife.CreationTime > timedLife.LifeTime)
                {
                    CommandBuffer.DestroyEntity(index, entity);
                }
            }
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var job = new TimedLifeJob
            {
                Time = Time.time,
                CommandBuffer = entityCommandBufferSystem.CreateCommandBuffer().ToConcurrent()
            }.Schedule(this, inputDeps);

            entityCommandBufferSystem.AddJobHandleForProducer(job);

            return job;
        }
    }
}