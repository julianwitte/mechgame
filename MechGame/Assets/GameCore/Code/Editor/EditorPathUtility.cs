﻿using UnityEngine;
using UnityEditor;
using System.IO;

public class EditorPathUtility : ScriptableObject
{
    public static string ActiveFolder()
    {
        var path = "";
        var obj = Selection.activeObject;
        if (obj == null)
            path = "Assets";
        else
            path = AssetDatabase.GetAssetPath(obj);

        if (!string.IsNullOrEmpty(path))
        {
            if (!Directory.Exists(path))
            {
                var splittedPath = path.Split('/');
                path.Replace(splittedPath[splittedPath.Length - 1], "");
            }
        }
        else
        {
            Debug.LogWarning("Could not specify selection path. Not in Assets?");
        }
        return path;
    }
}