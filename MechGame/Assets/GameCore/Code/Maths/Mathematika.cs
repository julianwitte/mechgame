﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;

public static class Mathematika
{
    public static float3 QuaternionToEulerXYZ(quaternion q)
    {
        float3 e = new float3(
            math.atan2(2f * q.value.x * q.value.w + 2f * q.value.y * q.value.z, 1 - 2f * (math.sqrt(q.value.z) + math.sqrt(q.value.w))),
            math.asin(2f * (q.value.x * q.value.z - q.value.w * q.value.y)),
            math.atan2(2f * q.value.x * q.value.y + 2f * q.value.z * q.value.w, 1 - 2f * (math.sqrt(q.value.y) + math.sqrt(q.value.z)))
            );
        return e;
    }

    public static float MoveTowards(float a, float b, float maxDelta)
    {
        var amount = b - a;
        if(math.abs(amount) > maxDelta)
        {
            amount = math.sign(amount) * maxDelta;
        }
        return a + amount;
    }

    public static float Magnitude(float3 vector)
    {
        return math.sqrt(math.pow(vector.x, 2f) + math.pow(vector.y, 2f) + math.pow(vector.z, 2f));
    }

    public static float Deg2Rad()
    {
        return 0.0174533f;
    }

    public static float Rad2Deg()
    {
        return 57.2958f;
    }
}
