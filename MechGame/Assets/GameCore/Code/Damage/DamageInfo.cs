﻿using UnityEngine;
using System.Collections;

namespace GameCore.Damage
{
    public struct DamageInfo
    {
        internal DamageDealer DamageDealer;
        internal int DamageValue;
        internal float Time;
        
        public DamageInfo(DamageDealer damageDealer, int damage, float time)
        {
            this.DamageDealer = damageDealer;
            this.DamageValue = damage;
            this.Time = time;
        }
    }
}