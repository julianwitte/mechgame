﻿using Unity.Entities;
using System.Collections;

namespace GameCore.Damage
{
    [System.Serializable]
    public struct DamageEvent : IComponentData
    {
        public Entity Damaged;
        public Entity DamageDealer;

        public Entity EventEntity;

        public DamageEvent(Entity eventEntity, Entity damaged, Entity damageDealer)
        {
            EventEntity = eventEntity;
            Damaged = damaged;
            DamageDealer = damageDealer;
        }
    }
    public class DamageEventComponent : ComponentDataProxy<DamageEvent>
    {

    }
}