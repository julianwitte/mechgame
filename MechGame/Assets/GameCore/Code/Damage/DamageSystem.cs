﻿using Unity.Entities;

namespace GameCore.Damage
{
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    public class DamageSystem : ComponentSystem
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((Entity entity, ref DamageEvent damageEvent) =>
            {
                if (EntityManager.HasComponent<Damageable>(damageEvent.Damaged) &&
                    EntityManager.HasComponent<DamageDealer>(damageEvent.DamageDealer))
                {
                    var damageable = EntityManager.GetComponentData<Damageable>(damageEvent.Damaged);
                    var damageDealer = EntityManager.GetComponentData<DamageDealer>(damageEvent.DamageDealer);

                    damageable.Damage += (damageDealer.Damage - damageable.Armor) + damageDealer.ArmorPiercingDamage;
                    PostUpdateCommands.SetComponent(damageEvent.Damaged, damageable);
                }
                PostUpdateCommands.DestroyEntity(entity);
            });
        }
        
    }
}