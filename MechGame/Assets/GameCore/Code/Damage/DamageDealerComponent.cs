﻿using Unity.Entities;
using System.Collections;

namespace GameCore.Damage
{
    [System.Serializable]
    public struct DamageDealer : IComponentData
    {
        public int Damage;
        public int ArmorPiercingDamage;
    }

    public class DamageDealerComponent : ComponentDataProxy<DamageDealer>
    {
        
    }
}