﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

namespace GameCore.Damage
{
    [System.Serializable]
    public struct Damageable : IComponentData
    {
        public int Durability;
        public int Armor;
        public int Damage;
    }

    public class DamageableComponent : ComponentDataProxy<Damageable>
    {
    }
}