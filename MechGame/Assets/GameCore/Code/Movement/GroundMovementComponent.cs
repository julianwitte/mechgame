﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics.Authoring;
using UnityEngine;

namespace GameCore.Movement
{
    [System.Serializable]
    public struct GroundMovementData : IComponentData
    {
        public float MaxSpeedForward;
        public float MaxSpeedBackward;
        public float MaxSpeedStrafe;
        public float MaxSpeedTurn;

        public float AccelerationForward;
        public float AccelerationBackward;
        public float AccelerationStrafe;
        public float AccelerationTurn;

        public float3 CurrentVelocity;
        public float3 CurrentAngularVelocity;
    }

    [RequireComponent(typeof(MovementInputComponent), typeof(PhysicsBody))]
    public class GroundMovementComponent : ComponentDataProxy<GroundMovementData> { }
}