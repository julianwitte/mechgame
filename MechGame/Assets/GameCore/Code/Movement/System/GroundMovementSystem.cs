﻿using Unity.Burst;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Transforms;
using UnityEngine;

namespace GameCore.Movement
{
    public class GroundMovementSystem : JobComponentSystem
    {
        [BurstCompile]
        struct GroundMovementJob : IJobForEach<GroundMovementData, MovementInput, Rotation, PhysicsVelocity>
        {
            public float deltaTime;

            public void Execute(ref GroundMovementData movementData, ref MovementInput input, ref Rotation rotation, ref PhysicsVelocity velocity)
            {
                // Translation
                //movementInput.Normalize();
                var translationInput = input.TranslationAxis;
                translationInput.y = 0f;

                if (Mathematika.Magnitude(translationInput) > 1f)
                    translationInput = math.normalize(translationInput);

                var currentLocalVelocity = math.rotate(math.inverse(rotation.Value), movementData.CurrentVelocity);

                var targetLocalVelocity = new float3(translationInput.x * movementData.MaxSpeedStrafe, 0f, translationInput.z * ((translationInput.z > 0f) ? movementData.MaxSpeedForward : movementData.MaxSpeedBackward));
                currentLocalVelocity.x = Mathematika.MoveTowards(currentLocalVelocity.x, targetLocalVelocity.x, movementData.AccelerationStrafe * deltaTime);
                currentLocalVelocity.z = Mathematika.MoveTowards(currentLocalVelocity.z, targetLocalVelocity.z, ((targetLocalVelocity.z > currentLocalVelocity.z) ? movementData.AccelerationForward : movementData.AccelerationBackward) * deltaTime);

                movementData.CurrentVelocity = math.rotate(rotation.Value, currentLocalVelocity);
                movementData.CurrentVelocity.y = velocity.Linear.y;
                velocity.Linear = movementData.CurrentVelocity;

                // Rotation
                var rotationInput = input.RotationAxis;
                var currentLocalAngularVelocity = math.rotate(math.inverse(rotation.Value), movementData.CurrentAngularVelocity);
                var targetTurnSpeed = input.RotationAxis.x * movementData.MaxSpeedTurn * Mathematika.Deg2Rad(); // Radians or Degrees?
                currentLocalAngularVelocity.y = Mathematika.MoveTowards(currentLocalAngularVelocity.y, targetTurnSpeed, movementData.AccelerationTurn * deltaTime);
                movementData.CurrentAngularVelocity = math.rotate(rotation.Value, currentLocalAngularVelocity);
                movementData.CurrentAngularVelocity.x = 0f;
                movementData.CurrentAngularVelocity.z = 0f;
                velocity.Angular = movementData.CurrentAngularVelocity;
            }
        }

        protected override void OnStartRunning()
        {
            base.OnStartRunning();
        }

        protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            var job = new GroundMovementJob()
            {
                deltaTime = Time.deltaTime
            };

            return job.Schedule(this, inputDeps);
        }
    }
}