﻿using UnityEngine;
using System.Collections;
using Unity.Entities;
using Unity.Mathematics;

namespace GameCore.Movement
{
    [System.Serializable]
    public struct MovementInput : IComponentData
    {
        public float3 TranslationAxis;
        public float3 RotationAxis;
    }

    public class MovementInputComponent : ComponentDataProxy<MovementInput> { }
}